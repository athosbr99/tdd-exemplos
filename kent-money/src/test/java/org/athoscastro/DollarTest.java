package org.athoscastro;

import org.junit.Test;

import static org.junit.Assert.*;

public class DollarTest {

    @Test
    public void testMultiplicacao(){
        Dollar cinco = Money.dollar(5);
        assertEquals(Money.dollar(10), cinco.vezes(2));
        assertEquals(Money.dollar(15), cinco.vezes(3));
    }

    @Test
    public void testIgualdade() {
        assertTrue(Money.dollar(5).equals(Money.dollar(5)));
        assertFalse(Money.dollar(5).equals(Money.dollar(6)));
    }
}