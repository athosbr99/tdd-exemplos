package org.athoscastro;

import org.junit.Test;

import static org.junit.Assert.*;

public class FrancTest {

    @Test
    public void testMultilicacao() {
        Franc cinco = Money.franc(5);
        assertEquals(Money.franc(10), cinco.vezes(2));
        assertEquals(Money.franc(15), cinco.vezes(3));
    }

    @Test
    public void testIgualdade() {
        assertTrue(Money.franc(5).equals(Money.franc(5)));
        assertFalse(Money.franc(5).equals(Money.franc(6)));
        assertFalse(Money.franc(19).equals(Money.dollar(19)));
    }
}