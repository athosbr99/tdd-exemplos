package org.athoscastro;

public abstract class Money{
    protected int quantidade;
    protected String moeda;

    Money(int quantidade, String moeda){
        this.quantidade = quantidade;
        this.moeda = moeda;
    }

    public boolean equals(Object object) {
        Money money = (Money) object;
        return quantidade == money.quantidade && getClass().equals(money.getClass());
    }

    abstract Money vezes(int multiplicador);

    String moeda(){
        return moeda;
    }

    static Dollar dollar(int quantidade) {
        return new Dollar(quantidade, "USD");
    }

    static Franc franc(int quantidade) {
        return new Franc(quantidade, "CHF");
    }
}
