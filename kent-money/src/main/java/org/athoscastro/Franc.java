package org.athoscastro;

public class Franc extends Money{
    private String moeda;
    Franc(int quantidade, String moeda) {
        super(quantidade, moeda);
    }

    Money vezes(int multiplicador){
        return Money.franc(quantidade * multiplicador);
    }

    String moeda() {
        return moeda;
    }
}
