package org.athoscastro;

public class Dollar extends Money {
    private String moeda;

    Dollar(int quantidade, String moeda){
        super(quantidade, moeda);
    }

    public Dollar vezes(int multiplicador){
        return Money.dollar(quantidade * multiplicador);
    }

    String moeda() {
        return moeda;
    }
}
